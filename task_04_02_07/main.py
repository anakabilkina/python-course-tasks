# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_07.
#
# Выполнил: Кабилкина А. С.
# Группа: ИС
# E-mail: kabilkinaanastasia.@mail.ru


nums_sum = 0
nums_count = 0
x = int(input('Введите {}-е число: '. format(nums_count+1)))
while x != 0:
    nums_sum += x
    nums_count += 1
    x = int(input('Введите {}-е число: '. format(nums_count+1)))
print("Сумма: ",(nums_sum))
print("Количество: ",(nums_count))

# --------------
# Пример вывода:
#
# Введите 1-е число: 1
# Введите 2-е число: 2
# Введите 3-е число: 3
# Введите 4-е число: 4
# Введите 5-е число: 0
# Сумма = 10
# Количество = 4
#
# Введите 1-е число: 0
# Сумма = 0
# Количество = 0
