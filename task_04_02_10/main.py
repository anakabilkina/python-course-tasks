# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_10.
#
# Выполнил: Кабилкина А. С.
# Группа: ИС
# E-mail: kabilkinaanastasia@mail.ru


n =int(input("n = "))
def s(n):
	result = 0
	while n > 0:
		result += n % 10
		n //= 10
	return result

print("Сумма = ", s(n))
print("Количество = ", len(str(n)))

# --------------
# Пример вывода:
#
# n = 12345
# Сумма = 15
# Количество = 5
