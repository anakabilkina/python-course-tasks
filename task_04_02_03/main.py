# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_03.
#
# Выполнил: Кабилкина А. С.
# Группа: ИС
# E-mail: kabilkinaanastasia@mail.ru


a = int(input("ширина форточки:"))
b = int(input("высота форточки:"))
d = int(input("диаметр головы:"))

if d + 2 <= a and d + 2 <= b: 
    print ('да')

else : 
    print('нет')

# --------------
# Пример вывода:
#
# Ширина форточки: 5
# Высота форточки: 6
# Диаметр головы: 6
# Нет

# Ширина форточки: 6
# Высота форточки: 7
# Диаметр головы: 4
# Да
